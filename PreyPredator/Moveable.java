
public class Moveable {

	
	int x_pos = 0;
	int y_pos = 0;
	
	public Moveable(int x, int y) {
		this.x_pos = x;
		this.y_pos = y;
	}
	
	public int get_x() {
		return x_pos;
	}
	public int get_y() {
		return y_pos;
	}
	public void set_x(int x) {
		x_pos = x;
	}
	public void set_y(int y) {
		y_pos = y;
	}
	
	public void print() {
		System.out.print("[" + x_pos + ";" + y_pos + "]");
	}
}
