import java.util.Random;
public class PredatorPrayTeleport {

	public static void main(String[] args) {
		
		runSimulation(10,2,20);
		
	}
	
	
	public static void checkBounds(Predator predator, Prey prey, int n) {
		
		if(predator.get_x() > n-1)
			predator.set_x(n-1);
		if(predator.get_y() > n-1)
			predator.set_y(n-1);
		if(predator.get_x() < 0)
			predator.set_x(0);
		if(predator.get_y() < 0)
			predator.set_y(0);
		
		if(prey.get_x() > n-1)
			prey.set_x(n-1);
		if(prey.get_y() > n-1)
			prey.set_y(n-1);
		if(prey.get_x() < 0)
			prey.set_x(0);
		if(prey.get_y() < 0)
			prey.set_y(0);
		
	}
	
	public static void print_pos(Prey prey, Predator predator) {
		prey.print();
		System.out.print(" ");
		predator.print();
		System.out.println();
		
	}
	
	
	public static boolean elidgeable_for_tp(Prey prey, int s) {
		return (prey.get_x() % s == 0 && prey.get_y() % s == 0);
	}
	
	public static void teleport(Prey prey, int x, int y){
		prey.set_x(x);
		prey.set_y(y);
	}
	
	
	
	public static void runSimulation(int n, int s, int t) {

		
		System.out.println("n=" + n + " s=" + s + " t=" + t);
		try {
			if(n < 0 || s < 2 || t <= 0) {
				throw new IllegalArgumentException("Illegal Parameter");
			}
		}
		catch(IllegalArgumentException i) {
			System.out.println("Illegal Parameters!");
			return;
		}
		
		Random rnd = new Random();
		

		Prey prey = new Prey(rnd.nextInt(n),rnd.nextInt(n));

		Predator predator = new Predator(rnd.nextInt(n),rnd.nextInt(n));
		
		
		print_pos(prey, predator);

		
		for(int i = 0; i < t; i++) {
			
			if(elidgeable_for_tp(prey, s))
				teleport(prey,rnd.nextInt(n),rnd.nextInt(n));
			else
				prey.move(rnd.nextInt(s)+1, rnd.nextInt(s)+1, rnd.nextInt(2));
			
			predator.move(s, prey);
			checkBounds(predator,prey,n);
			print_pos(prey, predator);
			

			
			if(predator.get_x() == prey.get_x() && predator.get_y() == prey.get_y()){
				System.out.println("Catch!");
				break;
			}
		}
	}
}