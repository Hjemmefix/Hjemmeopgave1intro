import java.util.Random;
public class PredatorPray {

	public static void main(String[] args) {
		
		runSimulation(10,0,20);
		
	}
	
	
	public static void checkBounds(Predator predator, Prey prey, int n) {
		
		if(predator.get_x() > n-1)
			predator.set_x(n-1);
		if(predator.get_y() > n-1)
			predator.set_y(n-1);
		if(predator.get_x() < 0)
			predator.set_x(0);
		if(predator.get_y() < 0)
			predator.set_y(0);
		
		if(prey.get_x() > n-1)
			prey.set_x(n-1);
		if(prey.get_y() > n-1)
			prey.set_y(n-1);
		if(prey.get_x() < 0)
			prey.set_x(0);
		if(prey.get_y() < 0)
			prey.set_y(0);
		
	}
	
	public static void print_pos(Prey prey, Predator predator) {
		prey.print();
		System.out.print(" ");
		predator.print();
		System.out.println();
		
	}
	
	
	
	public static void runSimulation(int n, int s, int t) {

		
		System.out.println("n=" + n + " s=" + s + " t=" + t);
		try {
			if(n <= 0 || s 	<= 0 || t < 0) {
				throw new IllegalArgumentException("Illegal Parameter");
			}
		}
		catch(IllegalArgumentException i) {
			System.out.println("Illegal Parameters!");
			return;
		}
		
		Random rnd = new Random();
		

		Prey prey = new Prey(rnd.nextInt(n)+1,rnd.nextInt(n)+1);

		Predator predator = new Predator(rnd.nextInt(n)+1,rnd.nextInt(n)+1);
		
		
		checkBounds(predator,prey,n);
		print_pos(prey, predator);

		
		for(int i = 0; i < t; i++) {
			
			prey.move(rnd.nextInt(s)+1, rnd.nextInt(s)+1, rnd.nextInt(2));
			predator.move(s, prey);
			checkBounds(predator,prey,n);
			print_pos(prey, predator);
			

			
			if(predator.get_x() == prey.get_x() && predator.get_y() == prey.get_y()){
				System.out.println("Catch!");
				break;
			}
		}
	}
}