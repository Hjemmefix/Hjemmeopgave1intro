
public class Prey extends Moveable {
	
	public Prey(int x, int y) {
		super(x,y);
	}
	
	
	public void move(int x, int y, int pn) {
		
		if(pn == 0) {
			x_pos += x;
			y_pos += y;
		}
		else {
			x_pos -= x;
			y_pos -= y;
		}
	}
}
