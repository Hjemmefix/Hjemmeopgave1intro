
public class Predator extends Moveable {

	public Predator(int x, int y) {
		super(x, y);
	}
	
	public void move(int s, Prey prey) {
		
		for(int i = 0; i < s; i++) {
			
			if(prey.get_x() > x_pos) {
				x_pos++;
			}
			else if(prey.get_x() < x_pos) {
				x_pos--;
			}
			if(prey.get_y() > y_pos) {
				y_pos++;
			}
			else if(prey.get_y() < y_pos){
				y_pos--;
			}
				
		}
		
	}
	

}
