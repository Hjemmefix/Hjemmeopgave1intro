
public class Moveable {

	
	static int x_pos = 0;
	static int y_pos = 0;
	
	public Moveable(int x, int y) {
		x_pos = x;
		y_pos = y;
	}
	
	public static int get_x() {
		return x_pos;
	}
	public static int get_y() {
		return y_pos;
	}
	public static void set_x(int x) {
		x_pos = x;
	}
	public static void set_y(int y) {
		y_pos = y;
	}
	
	public static void print() {
		System.out.println("(" + x_pos + ", " + y_pos + ")");
	}
}
