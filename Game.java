import java.util.Random;
public class Game {

	public static void main(String[] args) {
		
		runSimulation(23,3,100);
		
	}
	
	
	public static void checkBounds(Predator predator, Prey prey, int n) {
		
		if(predator.get_x() > n)
			predator.set_x(n);
		if(predator.get_y() > n)
			predator.set_y(n);
		if(predator.get_x() < 0)
			predator.set_x(0);
		if(predator.get_y() < 0)
			predator.set_y(0);
		
		if(prey.get_x() > n)
			prey.set_x(n);
		if(prey.get_y() > n)
			prey.set_y(n);
		if(prey.get_x() < 0)
			prey.set_x(0);
		if(prey.get_y() < 0)
			prey.set_y(0);
		
	}
	
	
	public static void runSimulation(int n, int s, int t) {
		
		Random rnd = new Random();
		
		
		Predator predator = new Predator(rnd.nextInt(n)+1, rnd.nextInt(n)+1);
		Prey prey = new Prey(rnd.nextInt(n)+1, rnd.nextInt(n)+1);
		
		checkBounds(predator,prey,n);
		
		
		
		System.out.print("Prey start: ");
		prey.print();
		System.out.print("Predator start:");
		predator.print();
		
		for(int i = 0; i < t; i++) {
			
			prey.move(rnd.nextInt(s)+1, rnd.nextInt(s)+1);
			predator.move(s, prey);
			checkBounds(predator,prey,n);
			prey.print();
			predator.print();
			

			
			if(predator.get_x() == prey.get_x() && predator.get_y() == prey.get_y()){
				System.out.println("Catch");
				break;
			}
		}
	}
}
