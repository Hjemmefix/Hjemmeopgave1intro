
public class Prey extends Moveable {
	
	static int x_pos = 0;
	static int y_pos = 0;
	public Prey(int x_cor, int y_cor) {
		super(x_cor,y_cor);
	}
	
	
	public static void move(int x, int y) {
		x_pos += x;
		y_pos += y;
	}
	
	

}
